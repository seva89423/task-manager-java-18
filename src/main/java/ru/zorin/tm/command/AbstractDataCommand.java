package ru.zorin.tm.command;

import ru.zorin.tm.dto.Domain;

import java.io.Serializable;

public abstract class AbstractDataCommand extends AbstractCommand implements Serializable {

    public AbstractDataCommand() {

    }

    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().getProjectList());
        domain.setTasks(serviceLocator.getTaskService().getTaskList());
        domain.setUsers(serviceLocator.getUserService().getUsersList());
        return domain;
    }

    public void setDomain(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().getProjectList().clear();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().getTaskList().clear();
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().getUsersList().clear();
        serviceLocator.getUserService().load(domain.getUsers());
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() throws Exception {

    }
}
