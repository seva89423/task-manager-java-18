package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidIndexException extends AbstractException {

    public InvalidIndexException() {
        super("Invalid index");
    }
}