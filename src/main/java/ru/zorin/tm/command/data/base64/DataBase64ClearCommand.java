package ru.zorin.tm.command.data.base64;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;
import java.io.File;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public String description() {
        return "Clear data from base64 file";
    }

    @Override
    public void execute() throws Exception {
    System.out.println("[DATA BASE64 DELETE]");
    final File file = new File(DataConst.FILE_BASE64);
    Files.deleteIfExists(file.toPath());
    System.out.println("[DATA IS CLEAR]");
    }
}