package ru.zorin.tm.repository;

import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public List<Project> getProjectList() {
        return projects;
    }

    @Override
    public void add(final List<Project> project) {
        for (final Project prt : project) {
            if (prt == null) return;
            projects.add(prt);
        }
    }

    @Override
    public void load(final List<Project> projects) {
        add(projects);
    }


    @Override
    public void remove(final String userId, Project project) {
        if (!userId.equals((project.getId()))) return;
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (Project project:projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> projects = findAll(userId);
        projects.removeAll(projects);
    }

    @Override
    public Project findProjectByIndex(final String userId, final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project getProjectByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        for (final Project project:projects){
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeProjectByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = getProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = findProjectByIndex(userId, index);
        if (project == null) throw new ProjectEmptyException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project findProjectById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        for (final Project task:projects){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Project removeProjectById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = findProjectById(userId, id);
        if (project == null) throw new ProjectEmptyException();
        projects.remove(project);
        return project;
    }
}