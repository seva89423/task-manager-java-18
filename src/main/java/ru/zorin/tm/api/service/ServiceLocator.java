package ru.zorin.tm.api.service;

import java.util.function.IntToDoubleFunction;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IDomainService getDomainService();

    IProjectService getProjectService();
}