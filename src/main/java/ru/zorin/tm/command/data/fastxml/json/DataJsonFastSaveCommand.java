package ru.zorin.tm.command.data.fastxml.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonFastSaveCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-fast-load";
    }

    @Override
    public String description() {
        return "Load data from json (FasterXML) file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA JSON (FAST) SAVE]");
        final String data = new String(Files.readAllBytes(Paths.get(DataConst.FILE_JSON_FAST)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        System.out.println("[COMPLETE]");
    }
}