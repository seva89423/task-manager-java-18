package ru.zorin.tm.api.service;

import ru.zorin.tm.role.Role;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void checkRole (Role[] roles);

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);
}