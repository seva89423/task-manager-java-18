package ru.zorin.tm.command.data.base64;

import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;

import java.io.*;
import java.nio.file.Files;;
import java.util.Base64;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-base64-save";
    }

    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");

        final Domain domain = getDomain();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final String base64 = Base64.getEncoder().encodeToString(bytes);
        byteArrayOutputStream.close();
        final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        objectOutputStream.close();
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[COMPLETE]");
    }
}