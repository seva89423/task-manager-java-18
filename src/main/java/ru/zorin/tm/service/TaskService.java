package ru.zorin.tm.service;

import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidIdException;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.invalid.InvalidNameException;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.error.task.TaskEmptyException;

import java.util.List;

public class TaskService implements ITaskService {

    final private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name){
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(final String userId, final String name, final String description){
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (task == null) throw new TaskEmptyException();
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (task == null) throw new TaskEmptyException();
        taskRepository.remove(userId, task);
    }

    @Override
    public void load(List<Task> tasks) {
        if (tasks == null) return;
        taskRepository.load(tasks);
    }


    public List<Task> getTaskList() {
        return taskRepository.getTaskList();
    }
    @Override
    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        return taskRepository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        taskRepository.clear(userId);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        return taskRepository.getTaskByName(userId, name);
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task =findOneById(userId, id);
        if (task == null) throw new TaskEmptyException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task removeOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
       if (index == null || index < 0) throw new InvalidIndexException();
       return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task removeOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task updateTaskByIndex(String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }
}