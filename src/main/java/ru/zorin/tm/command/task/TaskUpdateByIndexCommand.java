package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.error.task.TaskUpdateException;
import ru.zorin.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-task-by-index";
    }

    @Override
    public String description() {
        return "Update task by index";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
            if (task == null) throw new TaskEmptyException();
            System.out.println("ENTER NAME:");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Task taskUpdated = serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
            if (taskUpdated == null) throw new TaskUpdateException();
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }
}
