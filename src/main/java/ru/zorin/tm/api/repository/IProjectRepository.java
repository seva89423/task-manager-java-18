package ru.zorin.tm.api.repository;

import ru.zorin.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    List<Project> getProjectList();

    void add(final List<Project> project);

    void load(List<Project> projects);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findProjectById(String userId, String id);

    Project findProjectByIndex(String userId, Integer index);

    Project getProjectByName(String userId, String name);

    Project removeProjectByName(String userId, String name);

    Project removeProjectByIndex(String userId, Integer index);

    Project removeProjectById(String userId, String id);
}
