package ru.zorin.tm.command.data.jaxb.xml;

import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlClearCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-jaxb-clear";
    }

    @Override
    public String description() {
        return "Clear data from xml (Jax-B) file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML DELETE]");
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
        System.out.println("[COMPLETE]");
    }
}