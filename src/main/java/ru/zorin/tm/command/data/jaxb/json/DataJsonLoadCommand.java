package ru.zorin.tm.command.data.jaxb.json;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.constant.DataConst;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class DataJsonLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-jaxb-load";
    }

    @Override
    public String description() {
        return "Load data from json (Jax-B) file";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller unmarsh = jaxbContext.createUnmarshaller();
        final File file = new File(DataConst.FILE_JSON);
        unmarsh.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarsh.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        Domain domain = (Domain) unmarsh.unmarshal(file);
        setDomain(domain);
        System.out.println("[COMPLETE]");
    }
}