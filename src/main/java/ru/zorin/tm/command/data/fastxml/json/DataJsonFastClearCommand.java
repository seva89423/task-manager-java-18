package ru.zorin.tm.command.data.fastxml.json;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;

import java.io.File;
import java.nio.file.Files;

public class DataJsonFastClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-fast-clear";
    }

    @Override
    public String description() {
        return "Clear data from json (FasterXML) file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FAST) DELETE]");
        final File file = new File(DataConst.FILE_JSON_FAST);
        Files.deleteIfExists(file.toPath());
        System.out.println("[COMPLETE]");
    }
}
