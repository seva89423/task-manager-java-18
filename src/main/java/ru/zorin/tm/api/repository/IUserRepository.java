package ru.zorin.tm.api.repository;

import ru.zorin.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add (User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    List<User> getUsersList();

    void load(List<User> userList);

    void add(final List<User> userList);
}