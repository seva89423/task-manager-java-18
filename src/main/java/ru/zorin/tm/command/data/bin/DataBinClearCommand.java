package ru.zorin.tm.command.data.bin;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;

import java.io.File;
import java.nio.file.Files;

public class DataBinClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Clear data from bin file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN DELETE]");
        final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[COMPLETE]");
    }
}