package ru.zorin.tm.command.project;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "create-project";
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[COMPLETE]");
    }
}