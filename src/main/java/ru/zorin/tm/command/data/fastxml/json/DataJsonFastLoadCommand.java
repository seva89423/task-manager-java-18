package ru.zorin.tm.command.data.fastxml.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataJsonFastLoadCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-fast-save";
    }

    @Override
    public String description() {
        return "Save data to json (FasterXML) file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FAST) SAVE]");
        final Domain domain = getDomain();
        final File file = new File(DataConst.FILE_JSON_FAST);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[COMPLETE]");
    }
}