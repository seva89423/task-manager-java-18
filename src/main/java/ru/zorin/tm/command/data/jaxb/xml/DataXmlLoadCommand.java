package ru.zorin.tm.command.data.jaxb.xml;

import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.constant.DataConst;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-jaxb-save";
    }

    @Override
    public String description() {
        return "Save data to xml (Jax-B) file";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        System.out.println("[DATA XML SAVE]");
        final Domain domain = getDomain();
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        JAXBContext context = JAXBContext.newInstance(Domain.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(domain, new File(DataConst.FILE_XML_FAST));
        System.out.println("[COMPLETE]");
    }
}