package ru.zorin.tm.command.data.bin;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;

import java.io.*;

public class DataBinLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Load data from bin file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_BIN);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        fileInputStream.close();
        System.out.println("[COMPLETE]");
    }
}