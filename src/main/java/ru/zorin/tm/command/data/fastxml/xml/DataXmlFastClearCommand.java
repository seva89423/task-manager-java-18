package ru.zorin.tm.command.data.fastxml.xml;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlFastClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-fast-clear";
    }

    @Override
    public String description() {
        return "Clear data from xml (FasterXML) file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML (FAST) DELETE]");
        final File file = new File(DataConst.FILE_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[COMPLETE]");
    }
}