package ru.zorin.tm.command.data.jaxb.json;

import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.constant.DataConst;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataJsonSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-jaxb-save";
    }

    @Override
    public String description() {
        return "Save data to json (Jax-B) file";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        System.out.println("[DATA JSON SAVE]");
        final Domain domain = getDomain();
        final File file = new File(DataConst.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain, new File(DataConst.FILE_JSON));
        System.out.println("[COMPLETE]");
    }
}